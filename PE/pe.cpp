
// PE.cpp 
// 
// 2016.3.18
// by codetask 
// 
// compile： vs2015

#include <stdio.h>
#include <windows.h>
#include <list>
#include <string>
#include <commctrl.h>
#pragma  comment(lib,"comctl32.lib ")
#include "resource.h"

#pragma comment(lib, "Version.lib")
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")


/*
要增加区段的话:

addsection 后:
ntheader->FileHeader.NumberOfSections		+1
pe.ntheader->OptionalHeader.SizeOfImage		+size

*/

class PEFile
{
public:
    byte* filebuf;
    PIMAGE_DOS_HEADER dosheader;
    PIMAGE_NT_HEADERS ntheader;
    PIMAGE_SECTION_HEADER secheader;
    int seccount;

    PEFile() : filebuf( NULL ) {
    }
    ~PEFile() {
        if( filebuf )
            delete filebuf;
    }

    bool init( byte *infilebuf ) {
        dosheader = (PIMAGE_DOS_HEADER)infilebuf;
        if( dosheader->e_magic == 0x5A4D ) {
            ntheader = (PIMAGE_NT_HEADERS)(infilebuf + (DWORD)(dosheader->e_lfanew));
            secheader = (PIMAGE_SECTION_HEADER)(((DWORD)ntheader) + sizeof( IMAGE_NT_HEADERS ));
            seccount = ntheader->FileHeader.NumberOfSections;
            if( ntheader->Signature == IMAGE_NT_SIGNATURE ) {
                filebuf = infilebuf;
                return true;
            }
        }

        return false;
    }

};

class PEEditer
{
    struct BasicInfo
    {
        DWORD oep;
        DWORD fileope;
        DWORD imagebase;
        DWORD sizeofimage;
        DWORD subsystem;
        char  linkinfo[10];
        char  sysinfo[10];
    };

    struct PeSection
    {
        byte name[8];
        DWORD v_offset;
        DWORD v_size;
        DWORD raw_offset;
        DWORD raw_size;
        DWORD flags;
    };

    struct DirEntryInfo
    {
        DWORD export_rva;
        DWORD export_size;
        DWORD import_rva;
        DWORD import_size;
        DWORD resource_rva;
        DWORD resource_size;
    };

    struct ExportInfo
    {
        int   i;
        DWORD rva;
        DWORD raw;
        char  name[50];
    };

    struct ImportThunk
    {
        DWORD Ordinal;
        DWORD thunkrva;
        DWORD thunkraw;
        char name[50];
    };
    struct ImportInfo
    {
        char dllname[50];
        DWORD OriginalFirstThunk;
        DWORD FirstThunk;
        std::list<ImportThunk> pthunk;
    };

public:
    std::string curfile;
    DWORD filelen;
    DWORD filebase;

    PEFile pe;
    BasicInfo basicinfo;
    DirEntryInfo dirinfo;
    std::list<PeSection> pesecs;
    std::list<ExportInfo> pexportinfo;
    std::list<ImportInfo> pimportinfo;

    // init
    PEEditer() {
        puts( "PEEditer" );
    }
    ~PEEditer() {
        puts( "~PEEditer" );
    }

    //fn
    DWORD rvatoraw( DWORD rva ) {
        for( auto &it : pesecs )
            if( rva >= it.v_offset && rva <= it.v_offset + it.v_size )
                return (rva - it.v_offset + it.raw_offset);
        return 0;
    }

    // load info
    void loadsection() {
        //sections [first init]
        pesecs.clear();
        for( int i = 0; i < pe.seccount; i++ ) {
            auto tmpsec = pe.secheader + i;
            PeSection ps;
            memcpy( ps.name, tmpsec->Name, 8 );
            ps.v_offset = tmpsec->VirtualAddress;
            ps.v_size = tmpsec->Misc.VirtualSize;
            ps.raw_offset = tmpsec->PointerToRawData;
            ps.raw_size = tmpsec->SizeOfRawData;
            ps.flags = tmpsec->Characteristics;
            pesecs.push_back( ps );
        }
    }
    void loadbasic() {
        basicinfo.oep = pe.ntheader->OptionalHeader.AddressOfEntryPoint;
        basicinfo.fileope = rvatoraw( pe.ntheader->OptionalHeader.AddressOfEntryPoint );
        basicinfo.imagebase = pe.ntheader->OptionalHeader.ImageBase;
        basicinfo.sizeofimage = pe.ntheader->OptionalHeader.SizeOfImage;
        basicinfo.subsystem = pe.ntheader->OptionalHeader.Subsystem;
        wsprintfA( basicinfo.linkinfo, "%d.%d",
            pe.ntheader->OptionalHeader.MajorLinkerVersion,
            pe.ntheader->OptionalHeader.MinorLinkerVersion );
        wsprintfA( basicinfo.sysinfo, "%d.%d",
            pe.ntheader->OptionalHeader.MajorOperatingSystemVersion, pe.
            ntheader->OptionalHeader.MinorOperatingSystemVersion );
    }
    void loaddirectory() {
        //directory entry
        dirinfo.export_rva = pe.ntheader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress;
        dirinfo.export_size = pe.ntheader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].Size;
        dirinfo.import_rva = pe.ntheader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress;
        dirinfo.import_size = pe.ntheader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size;
        dirinfo.resource_rva = pe.ntheader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_RESOURCE].VirtualAddress;
        dirinfo.resource_size = pe.ntheader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_RESOURCE].Size;
    }
    void loadexport() {
        pexportinfo.clear();

        PIMAGE_EXPORT_DIRECTORY pied = (PIMAGE_EXPORT_DIRECTORY)(filebase + rvatoraw( dirinfo.export_rva ));
        if( pied ) {
            PDWORD fntable = (PDWORD)(filebase + rvatoraw( pied->AddressOfFunctions ));
            PDWORD namestable = (PDWORD)(filebase + rvatoraw( pied->AddressOfNames ));

            for( int i = 0; i < pied->NumberOfFunctions; i++ ) {
                PCH name = "-";
                if( i < pied->NumberOfNames )
                    name = (PCH)(filebase + rvatoraw( namestable[i] ));

                ExportInfo info = {i,fntable[i],rvatoraw( fntable[i] ),NULL};
                strcpy_s( info.name, 50, name );
                pexportinfo.push_back( info );
            }
        }
    }
    void loadimport() {
        pimportinfo.clear();

        PIMAGE_IMPORT_DESCRIPTOR piid = (PIMAGE_IMPORT_DESCRIPTOR)(filebase + rvatoraw( dirinfo.import_rva ));
        while( piid->OriginalFirstThunk ) {
            ImportInfo importinfo;
            strcpy( importinfo.dllname, (PCH)(filebase + rvatoraw( piid->Name )) );
            importinfo.OriginalFirstThunk = piid->OriginalFirstThunk;
            importinfo.FirstThunk = piid->FirstThunk;

            DWORD iatoffset = rvatoraw( piid->FirstThunk );
            PIMAGE_THUNK_DATA piat = (PIMAGE_THUNK_DATA)(filebase + iatoffset);
            PIMAGE_THUNK_DATA pitd = (PIMAGE_THUNK_DATA)(filebase + rvatoraw( piid->OriginalFirstThunk ));

            for( int i = 0;; i += 4 ) {
                if( pitd->u1.Ordinal == 0 )
                    break;

                ImportThunk importthunk;
                importthunk.thunkrva = piid->FirstThunk + i;
                importthunk.thunkraw = iatoffset + i;

                if( pitd->u1.Ordinal >> 31 == 1 ) {
                    //the import-func have no name
                    importthunk.Ordinal = pitd->u1.Ordinal & 0xFFFF;
                    strcpy( importthunk.name, "By Ordinal" );
                }
                else {
                    PIMAGE_IMPORT_BY_NAME piibn = (PIMAGE_IMPORT_BY_NAME)(filebase + rvatoraw( pitd->u1.AddressOfData ));
                    importthunk.Ordinal = piibn->Hint;
                    strcpy( importthunk.name, piibn->Name );
                }

                importinfo.pthunk.push_back( importthunk );
                pitd++;
            }

            pimportinfo.push_back( importinfo );
            piid++;
        }
    }
    // load file
    bool loadfile( const char* filename ) {
        curfile = filename;
        FILE * f = fopen( filename, "rb" );
        if( f ) {
            fseek( f, 0, SEEK_END );
            filelen = ftell( f );
            byte *filebuf = new byte[filelen];
            filebase = (DWORD)filebuf;
            fseek( f, 0, SEEK_SET );
            fread( filebuf, filelen, 1, f );
            fclose( f );

            if( pe.init( filebuf ) ) {
                //fill info
                loadsection();
                loadbasic();
                loaddirectory();
                loadexport();
                loadimport();
                return true;
            }

            delete filebuf;
            puts( "not a valid pe file" );
        }

        puts( "loadfile err" );
        return false;
    }
};






//////////////////////////////////////////////////////////////////////////
PEEditer g_peediter;
//////////////////////////////////////////////////////////////////////////

//click import button ->
BOOL CALLBACK importcallback( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam ) {

    switch( uMsg ) {
        case WM_INITDIALOG: {
            SetWindowTextA( hDlg, "Import Table" );
            HWND hlist1 = GetDlgItem( hDlg, IDC_LIST1 );
            ListView_SetExtendedListViewStyleEx( hlist1, 0, LVS_EX_FULLROWSELECT );
            LV_COLUMN colmn = {0};
            colmn.mask = LVCF_WIDTH | LVCF_TEXT;
            colmn.pszText = "FirstThunk";
            colmn.cx = 140;
            ListView_InsertColumn( hlist1, 0, &colmn );
            colmn.pszText = "OriginalFirstThunk";
            colmn.cx = 140;
            ListView_InsertColumn( hlist1, 0, &colmn );
            colmn.pszText = "Name";
            colmn.cx = 150;
            ListView_InsertColumn( hlist1, 0, &colmn );

            for( auto &it : g_peediter.pimportinfo ) {
                int cis = ListView_GetItemCount( hlist1 );
                LV_ITEMA item = {0};
                item.mask = LVIF_TEXT | LVIF_IMAGE;
                item.iItem = cis;
                item.pszText = it.dllname;
                cis = ListView_InsertItem( hlist1, &item );
                auto setitemt = [=]( int i, DWORD val ) {
                    char tmp[100];
                    wsprintfA( tmp, "%08X", val );
                    ListView_SetItemText( hlist1, cis, i, tmp );
                };
                setitemt( 1, it.OriginalFirstThunk );
                setitemt( 2, it.FirstThunk );
            }

            HWND hlist2 = GetDlgItem( hDlg, IDC_LIST2 );
            ListView_SetExtendedListViewStyleEx( hlist2, 0, LVS_EX_FULLROWSELECT );
            colmn.mask = LVCF_WIDTH | LVCF_TEXT;
            colmn.pszText = "Thunk Offset";
            colmn.cx = 110;
            ListView_InsertColumn( hlist2, 0, &colmn );
            colmn.pszText = "Thunk RVA";
            colmn.cx = 110;
            ListView_InsertColumn( hlist2, 0, &colmn );
            colmn.pszText = "API Name";
            colmn.cx = 120;
            ListView_InsertColumn( hlist2, 0, &colmn );
            colmn.pszText = "Hint/Ordinal";
            colmn.cx = 90;
            ListView_InsertColumn( hlist2, 0, &colmn );
            break;
        }

        case WM_NOTIFY: {
            //listview changed message :  WM_NOTIFY -> LVN_ITEMCHANGED
            //NMHDR nmhdr;
            //nmhdr.code = LVN_ITEMCHANGED;
            //nmhdr.idFrom = controlId;
            //nmhdr.hwndFrom = controlWindowHandle;
            //SendMessage( targetWindowHandle, WM_NOTIFY, controlId, &nmhdr );
            NMHDR* pnmhdr = (NMHDR*)lParam;
            HWND hlist1 = GetDlgItem( hDlg, IDC_LIST1 );
            NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pnmhdr;

            if( pnmhdr && pnmhdr->code == LVN_ITEMCHANGED && pnmhdr->hwndFrom == hlist1 &&
                (pNMListView->uChanged & LVIF_STATE) && (pNMListView->uNewState & LVIS_SELECTED) ) {

                int curselected = pNMListView->iItem;
                if( curselected != -1 ) {
                    char dllname[50];
                    ListView_GetItemText( hlist1, curselected, 0, dllname, 50 );
                    printf( "%s\n", dllname );

                    for( auto &it : g_peediter.pimportinfo ) {
                        if( strcmp( it.dllname, dllname ) == 0 ) {
                            HWND hlist2 = GetDlgItem( hDlg, IDC_LIST2 );
                            ListView_DeleteAllItems( hlist2 );

                            for( auto &it2 : it.pthunk ) {
                                int cis = ListView_GetItemCount( hlist2 );
                                LV_ITEMA item = {0};
                                item.mask = LVIF_TEXT | LVIF_IMAGE;
                                item.iItem = cis;
                                cis = ListView_InsertItem( hlist2, &item );
                                auto setitemt = [=]( int i, DWORD val ) {
                                    char tmp[100];
                                    wsprintfA( tmp, "%08X", val );
                                    ListView_SetItemText( hlist2, cis, i, tmp );
                                };
                                setitemt( 0, it2.Ordinal );
                                ListView_SetItemText( hlist2, cis, 1, it2.name );
                                setitemt( 2, it2.thunkrva );
                                setitemt( 3, it2.thunkraw );
                            }
                            break;
                        }
                    }
                }
            }
            break;
        }

        case WM_CLOSE:
            EndDialog( hDlg, 0 );
            return 1;
    }

    return 0;
}

//click export button ->
BOOL CALLBACK exportcallback( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam ) {

    switch( uMsg ) {
        case WM_INITDIALOG: {
            SetWindowTextA( hDlg, "Export Table" );
            HWND hlist = GetDlgItem( hDlg, IDC_LIST1 );
            ListView_SetExtendedListViewStyleEx( hlist, 0, LVS_EX_FULLROWSELECT );
            LV_COLUMN colmn = {0};
            colmn.mask = LVCF_WIDTH | LVCF_TEXT;
            colmn.pszText = "Name";
            colmn.cx = 170;
            ListView_InsertColumn( hlist, 0, &colmn );
            colmn.pszText = "Offset";
            colmn.cx = 80;
            ListView_InsertColumn( hlist, 0, &colmn );
            colmn.pszText = "Rva";
            colmn.cx = 80;
            ListView_InsertColumn( hlist, 0, &colmn );
            colmn.pszText = "Ordinal";
            colmn.cx = 80;
            ListView_InsertColumn( hlist, 0, &colmn );

            for( auto &it : g_peediter.pexportinfo ) {
                int cis = ListView_GetItemCount( hlist );
                LV_ITEMA item = {0};
                item.mask = LVIF_TEXT | LVIF_IMAGE;
                item.iItem = cis;
                cis = ListView_InsertItem( hlist, &item );
                auto setitemt = [=]( int i, DWORD val ) {
                    char tmp[100];
                    wsprintfA( tmp, "%08X", val );
                    ListView_SetItemText( hlist, cis, i, tmp );
                };
                setitemt( 0, it.i );
                setitemt( 1, it.rva );
                setitemt( 2, it.raw );
                ListView_SetItemText( hlist, cis, 3, it.name );
            }
            break;
        }

        case WM_CLOSE:
            EndDialog( hDlg, 0 );
            return 1;
    }

    return 0;
}


//////////////////////////////////////////////////////////////////////////
//start open file
void loadfile( HWND hDlg, char* filename ) {
    if( g_peediter.loadfile( filename ) == false )
        return;

    //File info
    char buf[50];
    wsprintfA( buf, "%d  bytes", g_peediter.filelen );
    SetDlgItemTextA( hDlg, IDC_EDIT15, buf );

    HANDLE hFile = CreateFileA( filename, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0 );
    if( hFile ) {
        FILETIME fCreateTime, fAccessTime, fWriteTime, localTime;
        SYSTEMTIME sysTime;
        if( GetFileTime( hFile, &fCreateTime, &fAccessTime, &fWriteTime ) ) {
            FileTimeToLocalFileTime( &fWriteTime, &localTime );
            FileTimeToSystemTime( &localTime, &sysTime );
            char timestr[100];
            sprintf( timestr, "%d-%d-%d %d:%d:%d", sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute, sysTime.wSecond );
            SetDlgItemTextA( hDlg, IDC_EDIT16, timestr );
        }
    }

    auto seteditdword = [=]( INT iEdit, DWORD val ) {
        char buf[30];
        wsprintfA( buf, "%08X", val );
        SetDlgItemTextA( hDlg, iEdit, buf );
    };
    //show basic info
    seteditdword( IDC_EDIT2, g_peediter.basicinfo.oep );
    seteditdword( IDC_EDIT3, g_peediter.basicinfo.fileope );
    seteditdword( IDC_EDIT5, g_peediter.basicinfo.imagebase );
    seteditdword( IDC_EDIT6, g_peediter.basicinfo.sizeofimage );
    seteditdword( IDC_EDIT8, g_peediter.basicinfo.subsystem );
    SetDlgItemTextA( hDlg, IDC_EDIT4, g_peediter.basicinfo.linkinfo );
    SetDlgItemTextA( hDlg, IDC_EDIT7, g_peediter.basicinfo.sysinfo );
    //show dirctory entry
    seteditdword( IDC_EDIT9, g_peediter.dirinfo.export_rva );
    seteditdword( IDC_EDIT10, g_peediter.dirinfo.export_size );
    seteditdword( IDC_EDIT11, g_peediter.dirinfo.import_rva );
    seteditdword( IDC_EDIT12, g_peediter.dirinfo.import_size );
    seteditdword( IDC_EDIT13, g_peediter.dirinfo.resource_rva );
    seteditdword( IDC_EDIT14, g_peediter.dirinfo.resource_size );
    //show sections
    HWND hlist = GetDlgItem( hDlg, IDC_LIST1 );
    ListView_DeleteAllItems( hlist );
    for( auto &it : g_peediter.pesecs ) {
        int cis = ListView_GetItemCount( hlist );
        LV_ITEMA item = {0};
        item.mask = LVIF_TEXT | LVIF_IMAGE;
        item.pszText = (char*)it.name;
        item.iItem = cis;
        cis = ListView_InsertItem( hlist, &item );
        auto setitemt = [=]( int i, DWORD val ) {
            char tmp[100];
            wsprintfA( tmp, "%08X", val );
            ListView_SetItemText( hlist, cis, i, tmp );
        };
        setitemt( 1, it.v_offset );
        setitemt( 2, it.v_size );
        setitemt( 3, it.raw_offset );
        setitemt( 4, it.raw_size );
        setitemt( 5, it.flags );
    }
}


void addSector()
{
	//增加区段 
	//1: pe.secheader +1 and edit
	//2: ntheader->FileHeader.NumberOfSections +1
	//3: sizeofimage +secsize
	//4: save file
}

//main wnd proc
BOOL CALLBACK mainproc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam ) {

    auto fileloadfn = [=]( char* path ) {
        SetDlgItemTextA( hDlg, IDC_EDIT1, path );
        loadfile( hDlg, path );
    };

    switch( uMsg ) {
        case WM_INITDIALOG: {
            HWND hlist = GetDlgItem( hDlg, IDC_LIST1 );
            ListView_SetExtendedListViewStyleEx( hlist, 0, LVS_EX_FULLROWSELECT );
            LV_COLUMN colmn = {0};
            colmn.mask = LVCF_WIDTH | LVCF_TEXT;
            colmn.pszText = "Flags";
            colmn.cx = 70;
            ListView_InsertColumn( hlist, 0, &colmn );
            colmn.pszText = "R.size";
            colmn.cx = 80;
            ListView_InsertColumn( hlist, 0, &colmn );
            colmn.pszText = "R.offset";
            colmn.cx = 90;
            ListView_InsertColumn( hlist, 0, &colmn );
            colmn.pszText = "V.size";
            colmn.cx = 80;
            ListView_InsertColumn( hlist, 0, &colmn );
            colmn.pszText = "V.offset";
            colmn.cx = 90;
            ListView_InsertColumn( hlist, 0, &colmn );
            colmn.pszText = "Name";
            colmn.cx = 80;
            ListView_InsertColumn( hlist, 0, &colmn );
            break;
        }

        case WM_DROPFILES: {
            HDROP hDropInfo = (HDROP)wParam;
            char szPath[260];
            DragQueryFileA( hDropInfo, 0, szPath, _countof( szPath ) );
            DragFinish( hDropInfo );
            fileloadfn( szPath );
            break;
        }

        case WM_COMMAND: {
            switch( LOWORD( wParam ) ) {
                //load file
                case IDC_BUTTON1:
					{
						char buf[260];

						OPENFILENAMEA ofn;
						ZeroMemory( &ofn, sizeof( ofn ) );
						ofn.lStructSize = sizeof( ofn );
						ofn.hwndOwner = hDlg;
						ofn.lpstrFile = buf;
						ofn.lpstrFile[0] = 0;
						ofn.nMaxFile = 260;
						ofn.lpstrFilter = "file";
						ofn.nFilterIndex = 1;
						ofn.nMaxFileTitle = 0;
						ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
						ofn.lpstrFileTitle = "choose";
						GetOpenFileNameA( &ofn );

						fileloadfn( buf );
					}
                    break;

                    //export table
                case IDC_BUTTON2:
                    DialogBox( 0, MAKEINTRESOURCE( IDD_LISTDIALOG ), (HWND)hDlg, exportcallback );
                    break;
                    //import table
                case IDC_BUTTON3:
                    DialogBox( 0, MAKEINTRESOURCE( IDD_IMPORTDIALOG ), (HWND)hDlg, importcallback );
                    break;

					//test
				case IDC_BUTTON5:
					break;
            }
            return 1;
        }

        case WM_CLOSE:
            EndDialog( hDlg, 0 );
            return 1;
    }

    return 0;
}

int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd ) {
    DialogBox( 0, MAKEINTRESOURCE( IDD_MAIN ), (HWND)0, mainproc );
    return 0;
}

